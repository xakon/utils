Utilities
=========

Various miscellaneous utilities that I need to keep up-to-date
(by-passing system's default versions), necessary for development
and working, in general.


List of Utilities
-----------------

 - [ack-grep](http://beyondgrep.com/) (3.5.0):
   A tool like grep(1), optimized for programmers.
   This version is a single-file version of the program, which includes
   all of its dependencies.

 - [cram](https://bitheap.org/cram/) (0.6):
   Cram is a functional testing framework for command-line applications
   based on [Mercurial](http://mercurial.selenic.com/)'s unified test format.

 - [virtualenv](https://virtualenv.pypa.io/) (13.2.0):
   Python Virtual Environments.  An absolute necessary tool for Python
   development.

 - [entr](http://eradman.com/entrproject/)
 - [dvtm](http://www.brain-dump.org/projects/dvtm/)
 - [fzf](https://github.com/junegunn/fzf)
 - [onefetch](https://snapcraft.io/onefetch)


Alternatives
------------

A [Perl] alternative to [cram](https://bitheap.org/cram/) is a CPAN module,
called [Test::CLI](https://metacpan.org/pod/Test::CLI).  It creates test cases
in [Perl], which we can even run in a test harness.

[Perl]:		https://perl.org
